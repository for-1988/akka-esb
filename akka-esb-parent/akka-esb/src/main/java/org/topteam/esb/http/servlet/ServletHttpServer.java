package org.topteam.esb.http.servlet;

import org.topteam.esb.common.URL;
import org.topteam.esb.http.AbstractHttpServer;
import org.topteam.esb.http.Handler;

public class ServletHttpServer extends AbstractHttpServer{

	public ServletHttpServer(URL url, Handler handler) {
		super(url, handler);
		DispatcherServlet.addHttpHandler(url.getPort(), handler);
	}

}
