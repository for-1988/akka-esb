package org.topteam.esb.protocol;

import org.topteam.esb.consumer.ConsumerBean;
import org.topteam.esb.provider.ProviderBean;

public interface Protocol {

	int getDefaultPort();
	
	String getProtocol();

	String getProtocolName();

	void provide(ProviderBean provider);

	<T> T consume(ConsumerBean consumer);

	void start();
}
