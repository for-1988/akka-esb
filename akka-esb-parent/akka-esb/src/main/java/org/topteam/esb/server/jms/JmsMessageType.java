package org.topteam.esb.server.jms;

public enum JmsMessageType {

	TEXT, OBJECT;
}
