package org.topteam.esb.http;

import org.topteam.esb.common.URL;

public interface Binder {

	HttpServer bind(URL url, Handler handler);
}
