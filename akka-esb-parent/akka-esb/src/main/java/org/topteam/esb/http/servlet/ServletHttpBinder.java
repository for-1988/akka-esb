package org.topteam.esb.http.servlet;

import org.topteam.esb.common.URL;
import org.topteam.esb.http.Binder;
import org.topteam.esb.http.Handler;
import org.topteam.esb.http.HttpServer;

public class ServletHttpBinder implements Binder {

	@Override
	public HttpServer bind(URL url, Handler handler) {
		 return new ServletHttpServer(url, handler);
	}

}
