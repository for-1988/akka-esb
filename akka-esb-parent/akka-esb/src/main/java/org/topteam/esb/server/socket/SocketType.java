package org.topteam.esb.server.socket;

public enum SocketType {
	TCP, UDP;
}
